#!/usr/bin/python
from pwn import *
from ctypes import *
import struct


if len(sys.argv) == 1:
	DEBUG = True
	libc = ELF("/lib/x86_64-linux-gnu/libc.so.6")
else:
	DEBUG = False
	libc = ELF("/lib/x86_64-linux-gnu/libc.so.6")


context.log_level 	=	"info"
context.bits		=	64
context.arch		=	"x86-64"
context.endian		=	"little"
context.os			=	"linux"
context.terminal  = ["tmux", "splitw", "-h"]

#/home/dlufy/rdebug/linux_serverx64
if DEBUG:
	r = process("./fibonacci")
else:
	r = remote("127.0.0.1", 2223)

GDB = 0
if GDB and DEBUG:
	gdb_cmd = []
	gdb_cmd.append("b *0x040092B")
	gdb_cmd.append("b *0x0400957")
	gdb_cmd.append("b *0x0000000000400a87")
	gdb_cmd.append("c")
	gdb.attach(r, "\n".join(gdb_cmd))


r.recvuntil("n = ")
r.sendline("21")
r.recvuntil("x1 = ")
r.sendline("4.466e-321")
r.recvuntil("x2 = ")
r.sendline("2.2e-321")

r.interactive()
